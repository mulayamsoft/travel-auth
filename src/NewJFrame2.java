//Albert Smith
//CSS 562
//Travel Authorization Week 3 Modified 
//21 January 2019

import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class NewJFrame2 extends javax.swing.JFrame {

    public NewJFrame2() {

        initComponents();

    }

    @SuppressWarnings("unchecked")

    private void initComponents() {

        // Create the labels using Java Swing
        label1 = new java.awt.Label();

        label2 = new java.awt.Label();

        label3 = new java.awt.Label();

        label4 = new java.awt.Label();

        label5 = new java.awt.Label();

        label6 = new java.awt.Label();

        jTextField1 = new javax.swing.JTextField();

        jTextField2 = new javax.swing.JTextField();

        jTextField3 = new javax.swing.JTextField();

        jTextField4 = new javax.swing.JTextField();

        jTextField5 = new javax.swing.JTextField();

        jTextField6 = new javax.swing.JTextField();

        jButton1 = new javax.swing.JButton();

        label7 = new java.awt.Label();

        labelCity = new java.awt.Label();
        // combo box for five different locations 
        jComboBoxCity = new JComboBox<String>();

        jRadioButtonCarCompany1 = new JRadioButton();

        jRadioButtonCarCompany2 = new JRadioButton();

        jRadioButtonCarCompany3 = new JRadioButton();

        buttonGroupCarCompanies = new ButtonGroup();

        jPanel = new JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        cities = new String[]{"New York", "New Orleans", "Denver", "Seattle", "Dallas"};

        foodCosts = new int[]{70, 80, 85, 90, 95};
        // assign label text and button listening event

        label1.setText("Traveler name ");

        label2.setText("Airfare cost ");

        label3.setText("Food cost ");

        label4.setText("Car cost ");

        label5.setText("Hotel cost ");

        label6.setText("Shuttle cost ");

        labelCity.setText("Location");

        jButton1.setText("Calculation Total");

        jRadioButtonCarCompany1.setText("AAA");

        jRadioButtonCarCompany2.setText("Hertz");

        jRadioButtonCarCompany3.setText("Enterprise");

        buttonGroupCarCompanies.add(jRadioButtonCarCompany1);
        buttonGroupCarCompanies.add(jRadioButtonCarCompany2);
        buttonGroupCarCompanies.add(jRadioButtonCarCompany3);

        jButton1.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(java.awt.event.ActionEvent evt) {

                jButton1ActionPerformed(evt);

            }

        });

        jComboBoxCity.addItemListener(new ItemListener() {

            @Override
            public void itemStateChanged(ItemEvent e) {
                // TODO Auto-generated method stub
                cmbCityActionPerformed(e);
            }
        });

        // populate combo box
        jComboBoxCity.setModel(new DefaultComboBoxModel<String>(cities));
        // Item changed event is no longer triggered on populate
        // now manually firing it to ensure Food Textbox is populated with the
        // corrresponding cost
        jComboBoxCity.setSelectedIndex(1);
        jComboBoxCity.setSelectedIndex(0);

        // Added layout for Label and Radio buttons in one panel.
        javax.swing.GroupLayout myl = new javax.swing.GroupLayout(jPanel);
        myl.setAutoCreateGaps(true);
        myl.setAutoCreateContainerGaps(true);
        myl.setVerticalGroup(myl.createSequentialGroup().addComponent(label5).addComponent(jRadioButtonCarCompany1)
                .addComponent(jRadioButtonCarCompany2).addComponent(jRadioButtonCarCompany3));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());

        getContentPane().setLayout(layout);

        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(123, 123, 123)
                                                .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, 409,
                                                        javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(layout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                                        .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        // replaced label5 with jPanel that holds the label together
                                                        // with radio buttons
                                                        .addComponent(jPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(jButton1)
                                                        .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(labelCity, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(179, 179, 179)
                                                .addGroup(layout
                                                        .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addComponent(jTextField1,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE, 184,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(layout
                                                                .createParallelGroup(
                                                                        javax.swing.GroupLayout.Alignment.TRAILING,
                                                                        false)
                                                                .addComponent(jTextField6,
                                                                        javax.swing.GroupLayout.Alignment.LEADING,
                                                                        javax.swing.GroupLayout.DEFAULT_SIZE, 70,
                                                                        Short.MAX_VALUE)
                                                                .addComponent(jTextField5,
                                                                        javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jTextField4,
                                                                        javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jTextField3,
                                                                        javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jComboBoxCity,
                                                                        javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jTextField2,
                                                                        javax.swing.GroupLayout.Alignment.LEADING)))))
                                .addGap(179, 179, 179)
                                .addContainerGap(123, Short.MAX_VALUE))
        );

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(label1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(label2, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(label3, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(29, 29, 29)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        // replaced label5 with jPanel that holds the label together with radio buttons
                                        .addComponent(jPanel, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(26, 26, 26)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(label4, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(label6, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(23, 23, 23)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(labelCity, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxCity, javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(28, 28, 28)
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 41,
                                        Short.MAX_VALUE)
                                .addComponent(label7, javax.swing.GroupLayout.PREFERRED_SIZE, 121,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(19, 19, 19))
        );

        pack();

    }
// update food field to add value based on location

    private void cmbCityActionPerformed(ItemEvent evt) {
        int index = jComboBoxCity.getSelectedIndex();
        int cost = foodCosts[index];
        jTextField3.setText(String.valueOf(cost));

    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

        // Declare variables to hold data. Set total to equal data values held in
        // arfare,food hotel, car and shuttle
        String Travelername = jTextField1.getText();

        double Airfarecost = Double.parseDouble(jTextField2.getText());

        double Foodcost = Double.parseDouble(jTextField3.getText());

        double Hotelcost = Double.parseDouble(jTextField4.getText());

        double Carcost = Double.parseDouble(jTextField5.getText());

        double Shuttlecost = Double.parseDouble(jTextField6.getText());

        double total = Airfarecost + Foodcost + Hotelcost + Carcost + Shuttlecost;

        // add ten percent of cost to total value by multiplying total by 0.1
        total = total + total * 0.1;

        label7.setText("The travel cost is " + total);

    }

    public static void main(String args[]) {

        try {

            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {

                if ("Nimbus".equals(info.getName())) {

                    javax.swing.UIManager.setLookAndFeel(info.getClassName());

                    break;

                }

            }

        } catch (ClassNotFoundException ex) {

            java.util.logging.Logger.getLogger(NewJFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);

        } catch (InstantiationException ex) {

            java.util.logging.Logger.getLogger(NewJFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);

        } catch (IllegalAccessException ex) {

            java.util.logging.Logger.getLogger(NewJFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {

            java.util.logging.Logger.getLogger(NewJFrame2.class.getName()).log(java.util.logging.Level.SEVERE, null,
                    ex);

        }

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {

                new NewJFrame2().setVisible(true);

            }

        });

    }

    private javax.swing.JButton jButton1;

    private javax.swing.JTextField jTextField1;

    private javax.swing.JTextField jTextField2;

    private javax.swing.JTextField jTextField3;

    private javax.swing.JTextField jTextField4;

    private javax.swing.JTextField jTextField5;

    private javax.swing.JTextField jTextField6;

    private java.awt.Label label1;

    private java.awt.Label label2;

    private java.awt.Label label3;

    private java.awt.Label label4;

    private java.awt.Label label5;

    private java.awt.Label label6;

    private java.awt.Label label7;

    private java.awt.Label labelCity;

    private javax.swing.JComboBox<String> jComboBoxCity;

    private javax.swing.JRadioButton jRadioButtonCarCompany1;

    private javax.swing.JRadioButton jRadioButtonCarCompany2;

    private javax.swing.JRadioButton jRadioButtonCarCompany3;

    private javax.swing.ButtonGroup buttonGroupCarCompanies;

    private javax.swing.JPanel jPanel;

    private String[] cities;
    private int[] foodCosts;

}
