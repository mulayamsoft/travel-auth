/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hp
 */
public class Travel {

    private String travelName;
    private int airFare;
    private int foodCost;
    private int noOfDaysTrip;
    private int hotelCost;
    private int carCost;
    private int shuttleCost;
    private String location;

    public Travel(String travelName, int airFare, int foodCost, int hotelCost, int carCost, int shuttleCost, String location, int noOfDaysTrip) {
        this.travelName = travelName;
        this.airFare = airFare;
        this.foodCost = foodCost;
        this.hotelCost = hotelCost;
        this.carCost = carCost;
        this.shuttleCost = shuttleCost;
        this.location = location;
        this.noOfDaysTrip = noOfDaysTrip;
    }

    public String getTravelName() {
        return travelName;
    }

    public void setTravelName(String travelName) {
        this.travelName = travelName;
    }

    public int getAirFare() {
        return airFare;
    }

    public void setAirFare(int airFare) {
        this.airFare = airFare;
    }

    public int getFoodCost() {
        return foodCost;
    }

    public void setFoodCost(int foodCost) {
        this.foodCost = foodCost;
    }

    public int getHotelCost() {
        return hotelCost;
    }

    public void setHotelCost(int hotelCost) {
        this.hotelCost = hotelCost;
    }

    public int getCarCost() {
        return carCost;
    }

    public void setCarCost(int carCost) {
        this.carCost = carCost;
    }

    public int getShuttleCost() {
        return shuttleCost;
    }

    public void setShuttleCost(int shuttleCost) {
        this.shuttleCost = shuttleCost;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
