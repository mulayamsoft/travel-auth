
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hp
 */
public class TravelService {

    /**
     * this method calculate total cost
     */
    //use of single parameter method to calculate cost
    public double calculateCost(Travel travel) {

        return travel.getAirFare() + travel.getCarCost() + travel.getFoodCost()
                + travel.getHotelCost() + travel.getShuttleCost();
    }

    /**
     * This method read cities from file.City name must be one city per line
     * with comma(,)
     *
     * @return List of city ,if file not found then return null
     */
    public List<String> findAllCities() {
        //create list to hold all cities from file
        List<String> list = null;
        //try with-resource will automatically close reader
        try (BufferedReader reader = new BufferedReader(new FileReader("src/cities.txt"))) {
            list = new ArrayList<>();
            String city = reader.readLine();
            while (city != null) {
                list.add(city);
                city = reader.readLine();
            }
        } catch (FileNotFoundException ex) {
            System.err.println("cities.txt file not found in current direcotry ");
        } catch (IOException ex) {
            System.err.println(ex);
        }
        return list;
    }

    public void save(String data) {
        try {
            FileWriter out = new FileWriter("data.txt", true);
            out.write(data);
            out.write("\n\n-----------------------------\n");
            System.out.println("Data Saved");
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TravelService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TravelService.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
